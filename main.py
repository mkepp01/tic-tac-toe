import pygame

pygame.init()

SCREEN_WIDTH = 400
SCREEN_HEIGHT = SCREEN_WIDTH

screen = pygame.display.set_mode((SCREEN_WIDTH, SCREEN_HEIGHT))
pygame.display.set_caption('TicTacToe')

# set frame rate
clock = pygame.time.Clock()
FPS = 60


# define colours
RED = (255, 0, 0)
BLUE = (0, 191, 255)
GREEN = (0, 128, 0)
BLACK = (0, 0, 0)
WHITE = (255, 255, 255)


# game variables
end_screen = False
run = True
player_round = True
RECT_SIZE = SCREEN_WIDTH / 5
game_map = [0, 0, 0, 0, 0, 0, 0, 0, 0]
winning_values = []

# load images
button_img = pygame.image.load("button.png")
button = pygame.transform.scale(button_img, (100, 50))
button_rect = button.get_rect()
button_rect.x = SCREEN_WIDTH / 2 - 50
button_rect.y = 10

# initialize rectangles
rect_list = []
col = 1
row = 1
for i in range(9):
    if col % 4 == 0:
        row += 1
        col = 1
    rect = pygame.rect.Rect(col * SCREEN_WIDTH / 5, row * SCREEN_HEIGHT / 5, RECT_SIZE, RECT_SIZE)
    rect_list.append(rect)
    col += 1


def draw_bg():
    # background color
    screen.fill(BLACK)

    # draw game model
    # horizontal lines
    pygame.draw.line(screen, WHITE, (1 * SCREEN_WIDTH / 5, 2 * SCREEN_HEIGHT / 5), (4 * SCREEN_WIDTH / 5,
                                                                                    2 * SCREEN_HEIGHT / 5))
    pygame.draw.line(screen, WHITE, (1 * SCREEN_WIDTH / 5, 3 * SCREEN_HEIGHT / 5), (4 * SCREEN_WIDTH / 5,
                                                                                    3 * SCREEN_HEIGHT / 5))

    # vertical lines
    pygame.draw.line(screen, WHITE, (2 * SCREEN_WIDTH / 5, 1 * SCREEN_HEIGHT / 5), (2 * SCREEN_WIDTH / 5,
                                                                                    4 * SCREEN_HEIGHT / 5))
    pygame.draw.line(screen, WHITE, (3 * SCREEN_WIDTH / 5, 1 * SCREEN_HEIGHT / 5), (3 * SCREEN_WIDTH / 5,
                                                                                    4 * SCREEN_HEIGHT / 5))

    # draw the needed shapes (circles or rectangles)
    for count, value in enumerate(game_map):
        if value == 1:
            pygame.draw.circle(screen, RED, (rect_list[count].x + RECT_SIZE / 2,
                                             rect_list[count].y + RECT_SIZE / 2), RECT_SIZE / 4)
        if value == 2:
            pygame.draw.rect(screen, BLUE, (rect_list[count].x + RECT_SIZE / 4, rect_list[count].y + RECT_SIZE / 4,
                                            RECT_SIZE / 2, RECT_SIZE / 2))


def mouse_clicked():
    global player_round
    for count, value in enumerate(rect_list):
        if value.collidepoint(pygame.mouse.get_pos()):
            if game_map[count] == 0:
                draw_shape(True, count)
                player_round = False
            return
    if button_rect.collidepoint(pygame.mouse.get_pos()):
        reset()


def reset():
    global player_round
    global end_screen
    global game_map
    game_map.clear()
    game_map = [0, 0, 0, 0, 0, 0, 0, 0, 0]
    end_screen = False
    player_round = True


def draw_shape(circle,  position):
    if circle:
        game_map[position] = 1
    else:
        game_map[position] = 2


def game_ai():
    # check if own win is possible
    own_list = game_map
    for count, value in enumerate(own_list):
        if value == 0:
            own_list[count] = 2
            if check_win():
                draw_shape(False, count)
                return
            else:
                own_list[count] = 0

    # check if win of enemy is possible
    enemy_list = game_map
    for count, value in enumerate(enemy_list):
        if value == 0:
            enemy_list[count] = 1
            if check_win():
                draw_shape(False, count)
                return
            else:
                enemy_list[count] = 0

    # look for the field with the most chances of winning the game
    win_counter = 0
    win_count = -1
    tester = True
    for count, value in enumerate(game_map):
        tmp_win_counter = 0
        if value == 0:
            tmp_x = count % 3
            tmp_y = count // 3
            # check if win in the horizontal line is possible
            for i in range(3):
                if game_map[i + tmp_y * 3] != 0:
                    tester = False
            if tester:
                tmp_win_counter += 1
            else:
                tester = True

            # check if win in the vertical line is possible
            for i in range(1, 3):
                if game_map[tmp_x + i * 3] != 0:
                    tester = False
            if tester:
                tmp_win_counter += 1
            else:
                tester = True

            # check if win in the diagonal line is possible
            if count == 4:
                if game_map[0] == 0 and game_map[8] == 0:
                    tmp_win_counter += 1

                if game_map[2] == 0 and game_map[6] == 0:
                    tmp_win_counter += 1

            if count == 0:
                if game_map[4] == 0 and game_map[8] == 0:
                    tmp_win_counter += 1

            if count == 2:
                if game_map[4] == 0 and game_map[6] == 0:
                    tmp_win_counter += 1

            if count == 6:
                if game_map[4] == 0 and game_map[2] == 0:
                    tmp_win_counter += 1

            if count == 8:
                if game_map[4] == 0 and game_map[0] == 0:
                    tmp_win_counter += 1

            if tmp_win_counter > win_counter:
                win_counter = tmp_win_counter
                win_count = count

    # draw the shape at the best location
    draw_shape(False, win_count)


def check_win():
    winning_values.clear()
    if game_map[0] != 0:
        if game_map[0] == game_map[1] and game_map[0] == game_map[2]:
            winning_values.extend((0, 1, 2))
            return True
        if game_map[0] == game_map[3] and game_map[0] == game_map[6]:
            winning_values.extend((0, 3, 6))
            return True
        if game_map[0] == game_map[4] and game_map[0] == game_map[8]:
            winning_values.extend((0, 4, 8))
            return True
    if game_map[1] == game_map[4] and game_map[1] == game_map[7] and game_map[1] != 0:
        winning_values.extend((1, 4, 7))
        return True
    if game_map[2] == game_map[5] and game_map[2] == game_map[8] and game_map[2] != 0:
        winning_values.extend((2, 5, 8))
        return True
    if game_map[3] == game_map[4] and game_map[3] == game_map[5] and game_map[3] != 0:
        winning_values.extend((3, 4, 5))
        return True
    if game_map[6] != 0:
        if game_map[6] == game_map[7] and game_map[6] == game_map[8]:
            winning_values.extend((6, 7, 8))
            return True
        if game_map[6] == game_map[4] and game_map[6] == game_map[2]:
            winning_values.extend((2, 4, 6))
            return True
    return False


def check_patt():
    for count, value in enumerate(game_map):
        if value == 0:
            return False
    return True


def draw_end_screen():
    if winning_values:
        pygame.draw.line(screen, RED, (rect_list[min(winning_values)].centerx, rect_list[min(winning_values)].centery),
                         (rect_list[max(winning_values)].centerx, rect_list[max(winning_values)].centery), 10)
    screen.blit(button, (button_rect.x, button_rect.y))


while run:
    clock.tick(FPS)

    if not end_screen:
        draw_bg()
        if check_patt():
            end_screen = True
            draw_end_screen()
        if check_win():
            end_screen = True
            draw_end_screen()

    if not player_round and not end_screen:
        game_ai()
        player_round = True

    # keyboard track events
    for event in pygame.event.get():
        # quit game
        if event.type == pygame.QUIT:
            run = False
        # keyboard presses
        if event.type == pygame.KEYDOWN:
            if event.key == pygame.K_ESCAPE:
                run = False

        # mouse
        if player_round or end_screen:
            if event.type == pygame.MOUSEBUTTONUP:
                if event.button == 1:
                    mouse_clicked()
    pygame.display.update()

pygame.quit()
